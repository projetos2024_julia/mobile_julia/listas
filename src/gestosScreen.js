import React from "react";
import { View, PanResponder, StyleSheet, Text, Dimensions } from "react-native";

const Gestos = ({ navigation }) => {

    const screenWidth = Dimensions.get('window').width;

    const panResponder = React.useRef(


        PanResponder.create({
            //Identifica o início do gesto
            onStartShouldSetPanResponder: () => true,
            //Identifica a execução do gesto (Movimento)
            onPanResponderMove: (event, gestureState) => {
                console.log('Movimento X:', gestureState.dx);
                console.log('Movimento Y:', gestureState.dy);
            },
            //Identifica o final do gesto
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dx > screenWidth / 2) {
                    //Verifica se passou da metade da tela
                    navigation.goBack();
                }

            }
        })

    ).current;

    return (
        <View style={styles.container} {...panResponder.panHandlers}>
            <Text style={styles.text}>Arrate para a direita!</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },

    text: {
        fontSize: 20,
        fontWeight: 'bold',
    }
})
export default Gestos;