import React from "react";
import { Button, View, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const HomeScreen = () => {
    const navigation = useNavigation();

    const handleUsers = () => {
        navigation.navigate('Users');
    }

    const handleGestos = () => {
        navigation.navigate('Gestos');
    }

    return (
        <View>
            <Button title="Ver Usuários" onPress={handleUsers} />
            <Button
                title="Ir para Aba Gestos"
                onPress={handleGestos}
                color={'blue'}
            />
        </View>

    );
};
export default HomeScreen;