import React from "react";
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "./src/homeScreen";
import UsersScreen from "./src/usersScreen";
import GestosScreen from "./src/gestosScreen";
import Gestos from "./src/gestosScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>

      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen}/>
        <Stack.Screen name="Users" component={UsersScreen}/>
        <Stack.Screen name="Gestos" component={GestosScreen}/>
      </Stack.Navigator>

    </NavigationContainer>
  );
}

